import 'package:shared_preferences/shared_preferences.dart';

class DBHelper {
  static DBHelper _helper = new DBHelper();

  static DBHelper getInstance() {
    return _helper;
  }

  void savesSudut(int sudut) async {
    var ref = await SharedPreferences.getInstance();
    ref.setInt("SUDUT", sudut);
  }

  void savesTime(String time, String key) async {
    var ref = await SharedPreferences.getInstance();
    ref.setString(key, time);
  }

  Future<int> getSudut() async {
    var ref = await SharedPreferences.getInstance();
    var sudut = ref.getInt("SUDUT") ?? 0;
    return sudut;
  }

  Future<String> getTime(String key) async {
    var ref = await SharedPreferences.getInstance();
    var time = ref.getString(key) ?? "";
    return time;
  }
}
