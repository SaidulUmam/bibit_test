import 'package:intl/intl.dart';

class DateTimeHelper {
  static DateTime format(String time) {
    // Date and Time Format
    final now = DateTime.now();
    final dateFormat = DateFormat('yyyy-MM-dd');
    final timeSpecific = time;
    final completeFormat = DateFormat('yyyy-MM-dd HH:mm:ss');

    // Today Format
    final todayDate = dateFormat.format(now);
    final todayDateAndTime = "$todayDate $timeSpecific";
    var resultToday = completeFormat.parseStrict(todayDateAndTime);
    if (now.isAfter(resultToday)) {
      resultToday = resultToday.add(Duration(hours: 12));
    }

    // Tomorrow Format
    var formatted = resultToday.add(Duration(hours: 12));
    final tomorrowDate = dateFormat.format(formatted);
    final tomorrowDateAndTime = "$tomorrowDate $timeSpecific";
    var resultTomorrow = completeFormat.parseStrict(tomorrowDateAndTime);
    return now.isAfter(resultToday) ? resultTomorrow : resultToday;
  }
}
