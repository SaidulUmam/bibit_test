import 'dart:io';

import 'package:android_alarm_manager_plus/android_alarm_manager_plus.dart';
import 'package:bibittest/helper/background_service.dart';
import 'package:bibittest/helper/navigation.dart';
import 'package:bibittest/helper/notification_helper.dart';
import 'package:bibittest/main.dart';
import 'package:bibittest/provider/jam_provider.dart';
import 'package:bibittest/scressns/diagram.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';

class Jam extends StatefulWidget {
  static const routeName = '/jam';

  @override
  _JamState createState() => _JamState();
}

class _JamState extends State<Jam> {
  final NotificationHelper _notificationHelper = NotificationHelper();
  final BackgroundService _service = BackgroundService();

  @override
  void initState() {
    initNotif();
    super.initState();
  }

  void initNotif() async {
    _service.initializeIsolate();
    if (Platform.isAndroid) {
      AndroidAlarmManager.initialize();
    }
    final NotificationAppLaunchDetails? notificationAppLaunchDetails =
        await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
    if (notificationAppLaunchDetails?.didNotificationLaunchApp ?? false) {
      Navigation.intentWithData(Diagram.routeName, DateTime.now());
    }
    await _notificationHelper
        .initNotifications(flutterLocalNotificationsPlugin);
  }

  @override
  void dispose() {
    selectNotificationSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var lebar = MediaQuery.of(context).size.width;
    return ChangeNotifierProvider(
        create: (context) => JamProvider(),
        child: Consumer<JamProvider>(builder: (context, st, _) {
          return Scaffold(
            body: Center(
              child: Container(
                padding: EdgeInsets.all(10),
                width: lebar - 20,
                height: lebar - 20,
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.orange),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () => st.simpanSudut(270, 9),
                          child: Text(
                            "09",
                            style: TextStyle(color: Colors.white, fontSize: 24),
                          ),
                        ),
                        InkWell(
                          onTap: () => st.simpanSudut(90, 3),
                          child: Text(
                            "03",
                            style: TextStyle(color: Colors.white, fontSize: 24),
                          ),
                        ),
                      ],
                    ),
                    _angka("04", "10", () => st.simpanSudut(120, 4),
                        () => st.simpanSudut(300, 10), 30, 330),
                    _angka("05", "11", () => st.simpanSudut(150, 5),
                        () => st.simpanSudut(330, 11), 60, 300),
                    _angka("07", "01", () => st.simpanSudut(210, 7),
                        () => st.simpanSudut(30, 1), 120, 240),
                    _angka("08", "02", () => st.simpanSudut(240, 8),
                        () => st.simpanSudut(60, 2), 150, 210),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () => st.simpanSudut(0, 0),
                          child: Text(
                            "12",
                            style: TextStyle(color: Colors.white, fontSize: 24),
                          ),
                        ),
                        InkWell(
                          onTap: () => st.simpanSudut(180, 6),
                          child: Text(
                            "06",
                            style: TextStyle(color: Colors.white, fontSize: 24),
                          ),
                        ),
                      ],
                    ),
                    _jarum(lebar, 130, 7, st.alarmSudut, Colors.yellowAccent),
                    _jarum(lebar, 90, 15, st.jam, Colors.green),
                    _jarum(lebar, 130, 10, st.menit, Colors.blue),
                    _jarum(lebar, 130, 5, st.detik, Colors.red),
                    Container(
                      height: 25,
                      width: 25,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.blueGrey,
                      ),
                    ),
                    Container(
                      height: 15,
                      width: 15,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white12,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        }));
  }

  _jarum(
      double lebarLayar, double tinggi, double lebar, int sudut, Color warna) {
    return Positioned(
      bottom: (lebarLayar - 40) / 2,
      child: RotationTransition(
        alignment: Alignment.bottomCenter,
        turns: AlwaysStoppedAnimation(sudut / 360),
        child: Container(
          height: tinggi,
          width: lebar,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            color: warna,
          ),
        ),
      ),
    );
  }

  _angka(String value1, String value2, void Function()? press1,
      void Function()? press2, int sudut, int posisi) {
    return RotationTransition(
      turns: AlwaysStoppedAnimation(sudut / 360),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: press2,
            child: RotationTransition(
              turns: AlwaysStoppedAnimation(posisi / 360),
              child: Text(
                value2,
                style: TextStyle(color: Colors.white, fontSize: 24),
              ),
            ),
          ),
          InkWell(
            onTap: press1,
            child: RotationTransition(
              turns: AlwaysStoppedAnimation(posisi / 360),
              child: Text(
                value1,
                style: TextStyle(color: Colors.white, fontSize: 24),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
