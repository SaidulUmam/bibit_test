import 'package:bibittest/provider/diagram_provider.dart';
import 'package:d_chart/d_chart.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Diagram extends StatelessWidget {
  static const routeName = '/diagram';
  final DateTime clickTime;

  Diagram(this.clickTime);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) => DiagramProvider(this.clickTime),
        child: Consumer<DiagramProvider>(builder: (context, st, _) {
          return Scaffold(
            body: Center(
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 30, right: 30, top: 70, bottom: 70),
                child: DChartBar(
                  data: [
                    {
                      'id': 'Bar',
                      'data': [
                        {
                          'domain': '${st.dateForCompare.hour}:00',
                          'measure': st.detik
                        },
                      ],
                    },
                  ],
                  yAxisTitle: 'Seconds',
                  xAxisTitle: 'Alarm Time',
                  domainLabelPaddingToAxisLine: 16,
                  axisLineTick: 2,
                  axisLinePointTick: 2,
                  axisLinePointWidth: 10,
                  axisLineColor: Colors.green,
                  measureLabelPaddingToAxisLine: 16,
                  barColor: (barData, index, id) => Colors.green,
                  showBarValue: true,
                ),
              ),
            ),
          );
        }));
  }
}
