import 'package:bibittest/helper/navigation.dart';
import 'package:bibittest/scressns/diagram.dart';
import 'package:bibittest/scressns/jam.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Bibit Test',
      navigatorKey: navigatorKey,
      initialRoute: Jam.routeName,
      routes: {
        Jam.routeName: (context) => Jam(),
        Diagram.routeName: (context) => Diagram(
              ModalRoute.of(context)!.settings.arguments as DateTime,
            ),
      },
    );
  }
}
