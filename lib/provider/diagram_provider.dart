import 'package:bibittest/helper/db_helper.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DiagramProvider extends ChangeNotifier {
  int detik = 0;
  final now = DateTime.now();
  final dateFormat = DateFormat('yyyy-MM-dd');
  final completeFormat = DateFormat('yyyy-MM-dd HH:mm:ss');
  String time1 = "";
  String time2 = "";
  final DateTime clickTime;
  DateTime dateForCompare = DateTime.now();

  DiagramProvider(this.clickTime) {
    getDetik();
  }

  getDetik() async {
    this.time1 = await DBHelper().getTime("TIME1");
    this.time2 = await DBHelper().getTime("TIME2");
    final todayDate = dateFormat.format(now);
    final dateTime1 = "$todayDate $time1";
    var resultToday1 = completeFormat.parseStrict(dateTime1);
    final dateTime2 = "$todayDate $time2";
    var resultToday2 = completeFormat.parseStrict(dateTime2);
    dateForCompare =
        clickTime.isAfter(resultToday2) ? resultToday2 : resultToday1;
    detik = clickTime.difference(dateForCompare).inSeconds;
    notifyListeners();
  }
}
