import 'dart:async';

import 'package:android_alarm_manager_plus/android_alarm_manager_plus.dart';
import 'package:bibittest/helper/background_service.dart';
import 'package:bibittest/helper/date_time_helper.dart';
import 'package:bibittest/helper/db_helper.dart';
import 'package:flutter/material.dart';

class JamProvider extends ChangeNotifier {
  var detik = DateTime.now().second * 6;
  var menit = DateTime.now().minute * 6;
  var jam = (DateTime.now().hour * 5 * 6) + ((DateTime.now().minute ~/ 12) * 6);
  int alarmSudut = 0;

  JamProvider() {
    _ganti();
    DBHelper().getSudut().then((value) {
      this.alarmSudut = value;
      notifyListeners();
    });
    Timer.periodic(Duration(seconds: 1), (timer) {
      _ganti();
    });
  }

  _ganti() {
    detik = DateTime.now().second * 6;
    menit = DateTime.now().minute * 6;
    jam = (DateTime.now().hour * 5 * 6) + ((DateTime.now().minute ~/ 12) * 6);
    notifyListeners();
  }

  simpanSudut(int sudut, int time) {
    this.alarmSudut = sudut;
    notifyListeners();
    String waktu = time < 10 ? "0$time:00:00" : "$time:00:00";
    String waktu2 = "${time + 12}:00:00";
    DBHelper.getInstance().savesSudut(sudut);
    DBHelper.getInstance().savesTime(waktu, "TIME1");
    DBHelper.getInstance().savesTime(waktu2, "TIME2");
    scheduledAlarm(waktu);
  }

  Future<bool> scheduledAlarm(String time) async {
    return await AndroidAlarmManager.periodic(
      Duration(hours: 12),
      2,
      BackgroundService.callback,
      startAt: DateTimeHelper.format(time),
      exact: true,
      wakeup: true,
    );
  }
}
